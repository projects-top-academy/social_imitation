﻿#include <iostream>
#include <vector>
#include <string>

class User;

class UsersGroup {
public:
    std::string name;
    std::vector<User*> users;

    UsersGroup(std::string groupName) : name(groupName) {}

    void addUser(User* user);
    void printUsers();
    void printUsersInCity(std::string cityName);
    void PrintUsersFromCity(const std::string& city_name);
};

class User {
public:
    std::string name;

    class Address {
    public:
        std::string address;
        std::string city;
    };

    Address userAddress;
    std::vector<UsersGroup*> groups;

    User(std::string userName) : name(userName) {}

    void joinGroup(UsersGroup* group);
    void printGroups();

    std::string GetCityName() {
        return userAddress.city;
    }

    void SetAddress(Address& newAddress) {
        userAddress = newAddress;
    }
};

void UsersGroup::addUser(User* user) {
    users.push_back(user);
    user->groups.push_back(this);
}

void UsersGroup::printUsers() {
    std::cout << "Users in group " << name << ":" << std::endl;
    for (User* user : users) {
        std::cout << user->name << std::endl;
    }
}

void UsersGroup::printUsersInCity(std::string cityName) {
    std::cout << "Users in group " << name << " living in " << cityName << ":" << std::endl;
    for (User* user : users) {
        if (user->GetCityName() == cityName) {
            std::cout << user->name << std::endl;
        }
    }
}

void UsersGroup::PrintUsersFromCity(const std::string& city_name) {
    std::cout << "Users in group " << name << " from city " << city_name << ":" << std::endl;
    for (User* user : users) {
        if (user->GetCityName() == city_name && user->name != "Bob") {
            std::cout << user->name << std::endl;
        }
    }
}

void User::joinGroup(UsersGroup* group) {
    groups.push_back(group);
    group->addUser(this);
}

void User::printGroups() {
    std::cout << "Groups of user " << name << ":" << std::endl;
    for (UsersGroup* group : groups) {
        std::cout << group->name << std::endl;
    }
}

int main() {
    User user1("Alice");

    User::Address address1;
    address1.address = "123 Main St";
    address1.city = "New York";

    user1.SetAddress(address1);

    UsersGroup group1("Gaming Community");
    user1.joinGroup(&group1);

    User user2("Bob");

    User::Address address2;
    address2.address = "456 Elm St";
    address2.city = "Paris";

    user2.SetAddress(address2);

    user2.joinGroup(&group1);

    group1.PrintUsersFromCity("Paris");

    return 0;
}